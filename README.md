# About

This is an interactive browser app for viewing live stock price data for 
individual stocks. The front end has been created using the React framework, and
the back end has been done with Django. A simple Python script scrapes current
stock price data off the web, which is then displayed via simple tickers within 
the web browser application.

# Usage

You'll first need to create an instance of a React app locally, as all of the 
React boilerplate is not included in this repository due to size issues. Once
that is done, move the files from this repository into that app directory. 
Navigate to the backend directory from this repository, and start the Django 
development server with:

```
python manage.py runserver
```

Then, navigate to the frontend directory and start the React development server
with:

```
npm start
```

This will serve the app locally on port 3000. Open a browser and enter the url
localhost:3000 to view and interact with the app. It is very simple and
self-explanatory to use. Once a ticker is loaded, it will update automatically at
regular intervals.

