import requests
from bs4 import BeautifulSoup
from datetime import datetime


# url template for yahoo finance data for a single stock
YAHOO_URL = (
    "https://au.finance.yahoo.com/quote/{}.AX?p={}.AX&.tsrc=fin-srch"
)
# identifiers for relevant data within html
TABLE_FORMATS = {
    'current': ('span', {'data-reactid': '15'}),
    'change': ('span', {'data-reactid': '16'})
}


def scrape_single_ticker(ticker):
    """
    """
    code = ticker['code']
    try:
        url = YAHOO_URL.format(code, code)
        # pull html from yahoo
        request = requests.get(url)
        soup = BeautifulSoup(request.text, 'html.parser')
        # pull current and change data from yahoo
        data = {}
        for element in TABLE_FORMATS:
            data[element] = soup.find(*TABLE_FORMATS[element]).get_text()
        # separate the change data
        change, changepc = data['change'].split(' ')
        change_float = float(change)
        # determine what colour the change text should be
        if change_float < 0.:
            colour = 'red'
        elif change_float > 0.:
            colour = 'green'
        else:
            colour = 'darkgrey'
        # update ticker data
        ticker['time'] = datetime.utcnow().strftime("%H:%M")
        ticker['current'] = "$" + data['current']
        ticker['change'] = change
        ticker['changepc'] = changepc
        ticker['exchange'] = "ASX"
        ticker['color'] = colour
        ticker['success'] = True
    except:
        ticker['success'] = False
    return ticker

        
def scrape(tickers):
    """
    """
    response = []
    for ticker in tickers:
            response.append(scrape_single_ticker(ticker))
    return response
