import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .scrape import scrape

@csrf_exempt
def index(request):
    tickers = json.loads(request.body)
    response = scrape(tickers)
    return JsonResponse(response, safe=False)
