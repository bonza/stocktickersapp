import React from 'react';
import Menu from './components/Menu';
import Content from './components/Content';
import nextId from 'react-id-generator';
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {input: '', tickers: []};
    };

    componentDidMount() {
        this.update = setInterval(
            () => {
                var response = this.state.tickers;
                if (response) {
                    this.handleRequest(response, false);
                }
            },
            60000
        );
    }

    componentWillUnmount() {
        clearInterval(this.update);
    }

    updateTickers = (response, addNew) => {
        if (addNew) {
            response = response[0];
            if (response.success) {
                response.id = nextId();
                var tickers = this.state.tickers.slice();
                tickers.unshift(response);
                this.setState({tickers: tickers});
            } else {
                alert("Not a valid code!")
            }
        } else {
            this.setState({tickers: response});
        }
    };

    handleRequest = (request, addNew) => {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "http://localhost:8000/scrape/", true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                this.updateTickers(JSON.parse(xhr.responseText), addNew);
            }
        }
        xhr.send(JSON.stringify(request));
    };

    handleClick = () => {
        const code = this.state.input;
        if (code) {
            this.setState({input: ''});
            const request = [{code: code.toUpperCase()}];
            this.handleRequest(request, true);
        }
    };

    deleteTicker = (id) => {
        var tickers = this.state.tickers.slice();
        this.setState({
            tickers: tickers.filter(ticker => 
                ticker.id !== id
            )
        });
    };

    handleChange = (e) => {
        const input = e.target.value;
        this.setState({input: input});    
    };

    handleEnter = (e) => {
        if (e.key === 'Enter') {
            this.handleClick();
        }
    };

    render() {
        return (
            <div className="App">
                <Menu 
                    handleChange={this.handleChange}
                    handleEnter={this.handleEnter}
                    handleClick={this.handleClick}
                    input={this.state.input}
                />
                <Content 
                    tickers={this.state.tickers}
                    deleteTicker={this.deleteTicker}
                />
            </div>
        );
    };
};

export default App;
