import React from 'react';

function TickerData(props) {
    const {time, code, current, change, changepc, exchange, color} = props.data;
    const altStyle = {
        marginTop: "-8px",
        float: "left",
        color: color,
        fontSize: "21px",
    };

    return (
        <div className="TickerDataBox">
            <p style={{
                marginBottom: "-8px",
                fontSize: "12px",
                color: "gray"
            }}>
                Last updated: {time} UTC
            </p>
            <p>
                <span style={{
                    textShadow: "1px 1px 2px rgb(159, 159, 159)",
                    fontSize: "38px",
                    fontWeight: "700"
                }}>
                    {code}&nbsp;
                </span>
                <span>{current} ({exchange})</span>
            </p>
            <p style={altStyle}>{change} <b>{changepc}</b></p>
        </div> 
    )
}

class CloseButton extends React.Component {
    constructor(props) {
        super(props);
        this.deleteTicker = this.props.deleteTicker.bind(
            this, this.props.id
        );
    }

    render() {
        return (
            <p 
                className="CloseButton"
                onClick={this.deleteTicker}
            >
                &#xd7;
            </p>
        );
    }
}

class Ticker extends React.Component {
    render() {
        return(
            <div className="Ticker">
                <TickerData data={this.props.data} />
                <CloseButton 
                    id={this.props.data.id}
                    deleteTicker={this.props.deleteTicker} 
                />
            </div>
        );
    }
}

class Content extends React.Component {
    render() {
        const tickers = this.props.tickers.map((ticker) => (
            <Ticker 
                key={ticker.id}
                data={ticker}
                deleteTicker={this.props.deleteTicker}
            />
        ));
        return (
            <div id="Content">
                {tickers}
            </div>
        );
    }
}

export default Content;