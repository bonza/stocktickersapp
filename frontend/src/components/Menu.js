import React from 'react';

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.props.handleChange.bind(this);
        this.handleEnter = this.props.handleEnter.bind(this);
    }

    render() {
        return(
            <input 
                id="TickerInput" 
                type="text"
                name="code"
                value={this.props.input}
                placeholder="Enter ticker symbol"
                onChange={this.handleChange}
                onKeyPress={this.handleEnter}
            />
        );
    }
}

class Button extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.props.handleClick;
    }

    render() {
        return(
            <div id="Button" onClick={this.handleClick}>&#10132;</div>
        )
    }
}

function Menu(props) {
    return(
        <div id="Menu">
            <div id="TickerInputBox">
                <Input 
                    handleChange={props.handleChange} 
                    handleEnter={props.handleEnter} 
                    input={props.input}
                />
                <Button handleClick={props.handleClick} />
            </div>
        </div>
    )
}

export default Menu;